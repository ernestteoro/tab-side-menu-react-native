
import React, { Component } from 'react';
import { 
    Button,
    StyleSheet,
    Text,
    View,
    Image } from 'react-native';
import HeaderButton from '../components/HeaderButton';

class Services extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { goBack } = this.props.navigation;
        return (
            <View style={styles.container}>
                <HeaderButton
                    icon="md-menu"
                    onPress={() => this.props.navigation.openDrawer()}
                />
                <Text style={styles.header}>
                    Hi, I'm a service page!
                </Text>
                <Button
                    onPress={() => goBack()}
                    title="Close Me"
                />
            </View>
        );
    }

}

export default Services
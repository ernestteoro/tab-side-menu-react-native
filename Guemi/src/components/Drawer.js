import React, { Component } from 'react';
import {
  Button,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import { 
  Container, 
  Header, 
  Content, 
  Body, 
  Card, 
  CardItem, 
  ListItem 
} from 'native-base';

export default class Drawer extends Component {

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Header style={{ height: 200 }}>
          <Image style={styles.imageStyle} source={require('../../assets/Person32.png')} />
        </Header>
        <Content>

          <ListItem>
            <Button
              onPress={() => navigate('Services')}
              title="Services"
            />
          </ListItem>
          <ListItem>
            <Button
              onPress={() => navigate('Events')}
              title="Events"
            />
          </ListItem>
          <ListItem>
            <Button
              onPress={() => navigate('Profile',{user:'Ernest'})}
              title="Profile"
            />
          </ListItem>
          
        </Content>
      </Container>
    );
  }

}



/*

<View style={styles.container}>
        <Text style={styles.header}>
          Drawer
        </Text>
        <Button
          onPress={() => navigate('Modal')}
          title="Open Modal"
        />
        <Button
          onPress={() => this.props.navigation.closeDrawer()}
          title="Close Me"
        />
      </View>
*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    //alignSelf: 'flex-start',
  },
  header: {
    fontSize: 20,
    marginVertical: 20,
  },
  imageStyle: {
    height: 150,
    width: 150,
    borderRadius: 75,
  },
});
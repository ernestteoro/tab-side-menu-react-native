

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import {
  DrawerNavigator,
  DrawerItems,
  TabNavigator,
  StackNavigator
} from 'react-navigation';

import Icon from 'react-native-vector-icons/Ionicons';
import Home from './src/tabs/Home';
import Settings from './src/tabs/Settings';
import Profile from './src/screens/Profile';
import Modal from './src/screens/Cadeau';
import Drawer from './src/components/Drawer';
import Cadeau from './src/screens/Cadeau';
import Services from './src/screens/Services';
import Events from './src/screens/Events';
import { Container, Header, Content, Body } from 'native-base';

const CustomDrawerNavigator = (props) => (
  <Container>
    <Header style={{ height: 200 }}>
      <Image style={styles.imageStyle} source={require('./assets/Person32.png')} />
    </Header>
    <Content>
      <DrawerItems {...props} />
    </Content>
  </Container>
)


// Stack navigation for Settings, profile and others
const SettingsTab = StackNavigator({
  Settings: {
    screen:Settings,
    navigationOptions: {
      header: null,               // Hide the header
      headerBackTitle: 'Back',    // Title back button Back when we navigate to Profile from Settings
    },
  },
  Profile: {
    screen:Profile,
    navigationOptions: ({ navigation }) => ({
      // Customize header's title with user name passed to navigate()
      // You can pass any props you'd like. For instance: navigate('Profile', { user: 'Ernest' }
      title: `${navigation.state.params.user}'s Profile`,
      header: null,
    }),
  },
  
}, {
    headerMode: 'screen',
  });

// Stack navigation for Home, services, event, gift screens
const HomeTab = StackNavigator({
  Home: {
    screen: Home, //Settings
    navigationOptions: {
      header: null,               // Hide the header
      headerBackTitle: 'Back',    // Title back button Back when we navigate to Profile from Settings
    },
  },
  Services:{
    screen:Services,
    navigationOptions: ({ navigation }) => ({
      // Customize header's title with user name passed to navigate()
      // You can pass any props you'd like. For instance: navigate('Profile', { user: 'Ernest' }
      title: ' Test for services',
      header: null,
    }),
  },
  Events:{
    screen:Events,
    navigationOptions: ({ navigation }) => ({
      // Customize header's title with user name passed to navigate()
      // You can pass any props you'd like. For instance: navigate('Profile', { user: 'Ernest' }
      title: ' Test for Events',
      header: null,
    }),
  },
}, {
    headerMode: 'screen',
  });

// Tab navigation for Home and Settings screens
const TabNavigation = TabNavigator({
  Home: {
    screen: HomeTab,
    navigationOptions: {
      tabBarLabel: 'Home',
      tabBarIcon: ({ tintColor, focused }) => <Icon
        name={'ios-home'}
        size={26}
        style={{ color: tintColor }}
      />
    },
  },
  Settings: {
    screen: SettingsTab,
    navigationOptions: {
      tabBarLabel: 'Settings',
      tabBarIcon: ({ tintColor, focused }) => <Icon
        name={'ios-settings'}
        size={26}
        style={{ color: tintColor }}
      />
    },
  },
  
});

// Wrap tab navigation into drawer navigation
const TabsWithDrawerNavigation = DrawerNavigator({
  Tabs: {
    screen: TabNavigation,
  },
}, {
    // Register custom drawer component
     contentComponent: props => <Drawer {...props} />
    //initialRouteName: 'Home',
    //contentComponent: CustomDrawerNavigator,
    //drawerOpenRoute: 'DrawerOpen',
    //drawerCloseRoute: 'DrawerClose',
    //drawerToggleRoute: 'DrawerToggle'
  });



// And lastly stack together drawer with tabs and modal navigation
// because we want to be able to call Modal screen from any other screen
export default StackNavigator({
  TabsWithDrawer: {
    screen: TabsWithDrawerNavigation,
  },
}, {
    // In modal mode screen slides up from the bottom
    //mode: 'modal',
    // No headers for modals. Otherwise we'd have two headers on the screen, one for stack, one for modal.
    headerMode: 'none',
  });

// Styling component

styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageStyle: {
    height: 150,
    width: 150,
    borderRadius: 75,
  }
});

